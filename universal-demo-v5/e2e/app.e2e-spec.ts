import { UniversalDemoV5Page } from './app.po';

describe('universal-demo-v5 App', function() {
  let page: UniversalDemoV5Page;

  beforeEach(() => {
    page = new UniversalDemoV5Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
